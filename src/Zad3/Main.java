package Zad3;

//Zadanie​ ​3:
//        Stwórz aplikację, która służy do przeglądania katalogów.
//        Przyjmuj w pętli linię z wejścia. Po odczytaniu linii przekaż ją do obiektu typu File, a
//        następnie wypisz o nim kolejno informacje:
//        Plik/katalog istnieje: Tak/Nie
//        Ścieżka relatywna:
//        Ścieżka absolutna:
//        Wielkość pliku/katalogu:
//        Ścieżka absolutna:
//        Data ostatniej modyfikacji:
//        Czy plik jest ukryty:
//        Prawo dostępu do odczytu:
//        Prawo dostępu do zapisu:
//        Prawo dostępu do wykonywania(Executable):
//        (Jeśli jest katalogiem)
//        Lista plików w tym katalogu (wraz z ich datą modyfikacji oraz rozmiarem [oddzielone
//        spacjami])
//        Rozszerzenie​ ​zadania:
//        Po napisaniu tej aplikacji, obsłuż komendy:
//        delete ŚCIEŻKA
//        create_folder ŚCIEŻKA
//        create_file ŚCIEŻKA
//        https://bitbucket.org/nordeagda2/simplefilecapabilities


import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String line = "";

        while (!line.equals("quit")) {
            System.out.print("Podaj komendę (delete/create_file/create_folder lub nazwa pliku którego informacje wypisać): ");
            line = sc.nextLine();
            String[] command = line.split(" ");
            String firstWord = command[0];
            String fileName = command[1];

            if (firstWord.toLowerCase().equals("delete")) {
                File file = new File(fileName);
                file.delete();
                if (!file.exists()) {
                    System.out.println("File has been deleted");
                } else {
                    System.out.println("ERROR");
                }
            } else if (firstWord.toLowerCase().equals("create_folder")) {
                File file = new File(fileName);
                file.mkdir();
                if (file.exists()) {
                    System.out.println("Directory has been created");
                } else {
                    System.out.println("ERROR");
                }
            } else if (firstWord.toLowerCase().equals("create_file")) {
                File file = new File(fileName);
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (file.exists()) {
                    System.out.println("File has been created");
                } else {
                    System.out.println("ERROR");
                }
            } else {

                File file = new File(firstWord);
                if (file.isFile()) {
                    if (file.exists()) {
                        System.out.println("Plik istnieje: TAK");
                    } else if (!file.exists()) {
                        System.out.println("Plik istnieje: NIE");
                    }
                    System.out.println("ścieżka absolutna: " + file.getAbsolutePath());
                    System.out.println("ścieżka relatywna: " + file.getPath());
                    System.out.println("Wielkosć pliku: " + file.length());
                    System.out.println("Data ostatniej modyfikacji: " + file.lastModified());
                    System.out.println("Czy plik jest ukryty: " + file.isHidden());
                    System.out.println("Prawo dostępu do odczytu: " + file.canRead());
                    System.out.println("Prawo dostępu do zapisu: " + file.canWrite());
                    System.out.println("Prawo dostępu do wykonania: " + file.canExecute());
                } else if (file.isDirectory()) {
                    if (file.exists()) {
                        System.out.println("Katalog istnieje: TAK");
                    } else if (!file.exists()) {
                        System.out.println("Katalog istnieje: NIE");
                    }
                    System.out.println("Rozmiar katlogu: " + file.length());


                    for (File number : file.listFiles()) {
                        System.out.println(number + " " + number.length());
                    }
                }
            }
        }
    }
}
